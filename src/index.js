/**
 * @module
 * @description postal object model public API
 */
export {default as PostalAddressFactory} from './postalAddressFactory';
export {default as PostalAddress } from './postalAddress'