import PostalAddress from './postalAddress';

export default class PostalAddressFactory {

    static construct(data):PostalAddress {

        const street = data.street;

        const city = data.city;

        const regionIso31662Code = data.regionIso31662Code;

        const postalCode = data.postalCode;

        const countryIso31661Alpha2Code = data.countryIso31661Alpha2Code;

        return new PostalAddress(
            street,
            city,
            regionIso31662Code,
            postalCode,
            countryIso31661Alpha2Code
        );

    }

}