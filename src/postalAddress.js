/**
 * @class {PostalAddress}
 */
export default class PostalAddress {

    _street:string;

    _city:string;

    _regionIso31662Code:string;

    _postalCode:string;

    _countryIso31661Alpha2Code:string;

    /**
     * @param {string} street
     * @param {string} city
     * @param {string} regionIso31662Code ISO 3166-2 codegi
     * @param {string} postalCode
     * @param {string} countryIso31661Alpha2Code ISO 3166-1-Alpha2 code
     */
    constructor(street:string,
                city:string,
                regionIso31662Code:string,
                postalCode:string,
                countryIso31661Alpha2Code:string) {

        if (!street) {
            throw new TypeError('street required');
        }
        this._street = street;

        if (!city) {
            throw new TypeError('city required');
        }
        this._city = city;

        if (!regionIso31662Code) {
            throw new TypeError('regionIso31662Code required');
        }
        this._regionIso31662Code = regionIso31662Code;

        if (!postalCode) {
            throw new TypeError('postalCode required');
        }
        this._postalCode = postalCode;

        if (!countryIso31661Alpha2Code) {
            throw new TypeError('countryIso31661Alpha2Code required');
        }
        this._countryIso31661Alpha2Code = countryIso31661Alpha2Code;

    }

    /**
     * @returns {string}
     */
    get street():string {
        return this._street;
    }

    /**
     * @returns {string}
     */
    get city():string {
        return this._city;
    }

    /**
     * @returns {string}
     */
    get regionIso31662Code():string {
        return this._regionIso31662Code;
    }

    /**
     * @returns {string}
     */
    get postalCode():string {
        return this._postalCode;
    }

    /**
     * @returns {string}
     */
    get countryIso31661Alpha2Code():string {
        return this._countryIso31661Alpha2Code;
    }

    toJSON() {
        return {
            street: this._street,
            city: this._city,
            regionIso31662Code: this._regionIso31662Code,
            postalCode: this._postalCode,
            countryIso31661Alpha2Code: this._countryIso31661Alpha2Code
        }
    }
}
