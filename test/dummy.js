import PostalAddress from '../src/postalAddress';

const dummy = {
    streetAddress: 'street',
    city: 'city',
    iso31662Code: 'WA',
    postalCode: 'postalCode',
    iso31661Alpha2Code: 'US'
};

dummy.postalAddress =
    new PostalAddress(
        dummy.streetAddress,
        dummy.city,
        dummy.iso31662Code,
        dummy.postalCode,
        dummy.iso31661Alpha2Code
    );

/**
 * dummy objects (see: http://xunitpatterns.com/Dummy%20Object.html)
 */
export default dummy;
