import PostalAddressFactory from '../../src/postalAddressFactory';
import PostalAddress from '../../src/postalAddress';
import dummy from '../dummy';

describe('PostalAddressFactory', () => {
    describe('construct method', () => {
        it('constructs expected PostalAddress', () => {
            /*
             arrange
             */
            const expectedPostalAddress =
                dummy.postalAddress;

            const postalAddressData = {
                street: expectedPostalAddress.street,
                city: expectedPostalAddress.city,
                regionIso31662Code: expectedPostalAddress.regionIso31662Code,
                postalCode: expectedPostalAddress.postalCode,
                countryIso31661Alpha2Code: expectedPostalAddress.countryIso31661Alpha2Code
            };

            /*
             act
             */
            const actualPostalAddress =
                PostalAddressFactory.construct(postalAddressData);

            /*
             assert
             */
            expect(actualPostalAddress).toEqual(actualPostalAddress);

        })
    })
});
