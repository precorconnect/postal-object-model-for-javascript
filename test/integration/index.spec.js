import {PostalAddress} from '../../src/index';
import dummy from '../dummy';

describe('Index module', () => {

    describe('PostalAddress export', () => {
        it('should be PostalAddress constructor', () => {

            /*
             act
             */
            const objectUnderTest =
                new PostalAddress(
                    dummy.streetAddress,
                    dummy.city,
                    dummy.iso31662Code,
                    dummy.postalCode,
                    dummy.iso31661Alpha2Code
                );

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(PostalAddress));

        });
    });

});