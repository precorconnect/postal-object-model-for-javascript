## Description
Postal object model for javascript.

## Models

#####Postal Address
A postal address

## Setup

**install via jspm**  
```shell
jspm install postal-object-model=bitbucket:precorconnect/postal-object-model-for-javascript
```

**import & use desired models**
```javascript
import {PostalAddress} from 'postal-object-model'

const postalAddress = new PostalAddress(/*params snipped*/);
```

## Platform Support

This library can be used in the **browser** and **node.js**.

## Develop

#### Software
- git
- npm

#### Scripts

install dependencies (perform prior to running or testing locally)
```PowerShell
npm install
```

unit & integration test in multiple browsers/platforms
```PowerShell
# note: following environment variables must be present:
# SAUCE_USERNAME
# SAUCE_ACCESS_KEY
npm test
```